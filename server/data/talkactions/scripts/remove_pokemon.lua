function onSay(player, words, param)
	if not player:getGroup():getAccess() then
		return true
	end

	if player:getAccountType() < ACCOUNT_TYPE_GOD then
		return false
	end

	if tonumber(param) <= 0 or tonumber(param) > 6 then
		return false
	end
	
	player:removePokemon(tonumber(param))

	return false
end
