local nextSlot = 1
pokebag = nil
pokebagPanel = nil
mapPanel = nil
activePokemon = nil
local pokebagMap = {}

local LifeBarColors = {} -- Must be sorted by percentAbove
table.insert(LifeBarColors, {
    percentAbove = 92,
    color = '#00BC00'
})
table.insert(LifeBarColors, {
    percentAbove = 60,
    color = '#50A150'
})
table.insert(LifeBarColors, {
    percentAbove = 30,
    color = '#A1A100'
})
table.insert(LifeBarColors, {
    percentAbove = 8,
    color = '#BF0A0A'
})
table.insert(LifeBarColors, {
    percentAbove = 3,
    color = '#910F0F'
})
table.insert(LifeBarColors, {
    percentAbove = -1,
    color = '#850C0C'
})

function init()
    mapPanel = modules.game_interface.getMapPanel()
    pokebag = g_ui.loadUI('game_pokebag', mapPanel)
    pokebagPanel = pokebag:getChildById('pokebagPanel')

    connect(g_game, {
        onGameStart = online,
        onGameEnd = offline,
        onGameStart = setup
    })
    connect(LocalPlayer, {
        onAddPokemon = addPokemon,
        onRemovePokemon = removePokemon,
        onGoPokemon = goPokemon,
        onBackPokemon = backPokemon,
        onDeathPokemon = deathPokemon,
        onHealPokemon = healPokemon,
    })
    connect(Creature, {
        onHealthPercentChange = pokemonChangeHealth,
    })
end

function terminate()
    pokebag:destroy()
    disconnect(g_game, {
        onGameStart = online,
        onGameEnd = offline,
        onGameStart = setup
    })
    disconnect(LocalPlayer, {
        onAddPokemon = addPokemon,
        onRemovePokemon = removePokemon,
        onGoPokemon = goPokemon,
        onBackPokemon = backPokemon,
        onDeathPokemon = deathPokemon,
        onHealPokemon = healPokemon,
    })
    disconnect(Creature, {
        onHealthPercentChange = pokemonChangeHealth,
    })
end

function setup()
    for v, k in pairs(pokebagPanel:getChildren()) do
        pokebagPanel:removeChild(k)
        pokebagMap[k:getId()] = nil
    end
    nextSlot = 0
    activePokemon = nil
    pokebag:setHeight(0)
end

function addPokemon(player, pokemon)
    if nextSlot > 6 then
        return false
    end
    
    pokeball = g_ui.createWidget('Pokemon', pokebagPanel)
    pokeball:setId(pokemon:getId())

    pokeballImage = pokeball:getChildById('pokemon')
    pokeballImage:setImageSource('/images/pokemon_icon/' .. ((pokemon:getId() % 361) + 1))

    if(pokemon:getHealthPercent() <= 0) then
        deathPokemon(nil, pokemon:getId())
    end

    pokemonChangeHealth(pokemon)

    if pokebagPanel:getChildCount() == 1 then
        pokeball:addAnchor(AnchorTop, 'parent', AnchorTop) 
    end

    table.insert(pokebagMap, pokemon:getId(), pokemon)
    nextSlot = nextSlot + 1

    pokebag:setHeight(pokebag:getHeight() + 5 + 4 + pokeball:getHeight())
end

function removePokemon(player, pokemonId)
    local removedPokemon = pokebagPanel:getChildById(pokemonId)
    pokebag:setHeight(pokebag:getHeight() - 5 - 4 - removedPokemon:getHeight())
    pokebagPanel:removeChild(removedPokemon)

    for v, k in pairs(pokebagPanel:getChildren()) do
        if v == 1 then
            k:addAnchor(AnchorTop, 'parent', AnchorTop)
        end
    end

    table.remove(pokebagMap, pokemonId)
    nextSlot = nextSlot - 1
end

function goPokemon(player, pokemonId)
    pokeball = pokebagPanel:getChildById(pokemonId)
    if pokeball == nil then
        return
    end
    pokemon = pokeball:getChildById('pokemon')
    pokemon:setBackgroundColor('green')
    pokemon:setOpacity('0.5')
    activePokemon = pokemonId
end

function backPokemon(player, pokemonId)
    pokeball = pokebagPanel:getChildById(pokemonId)
    if pokeball == nil then
        return
    end
    pokemon = pokeball:getChildById('pokemon')
    pokemon:setBackgroundColor('alpha')
    pokemon:setOpacity('1.0')
    activePokemon = nil
end

function deathPokemon(player, pokemonId)
    pokeball = pokebagPanel:getChildById(pokemonId)
    if pokeball == nil then
        return
    end
    pokemon = pokeball:getChildById('pokemon')
    pokemon:setBackgroundColor('red')
    pokemon:setOpacity('0.5')
end

function healPokemon(player, pokemonId)
    pokeball = pokebagPanel:getChildById(pokemonId)
    if pokeball == nil then
        return
    end
    if pokemonId ~= activePokemon then
        pokemon = pokeball:getChildById('pokemon')
        pokemon:setBackgroundColor('alpha')
        pokemon:setOpacity('1.0')
    end

    changePokemonLife(pokemonId, 100)
end

function pokemonChangeHealth(pokemon)
    changePokemonLife(pokemon:getId(), pokemon:getHealthPercent())
end

function goback(pokebag)
    g_game.gobackPokemon(pokebag:getId())
end

function changePokemonLife(pokemonId, percent)
    pokeball = pokebagPanel:getChildById(pokemonId)
    if pokeball == nil then
        return
    end
    pokeballLife = pokeball:getChildById('lifebar')
    pokeballLife:setPercent(percent)
    local color
    for i, v in pairs(LifeBarColors) do
        if pokeballLife:getPercent() > v.percentAbove then
            color = v.color
            break
        end
    end

    pokeballLife:setBackgroundColor(color)
end
