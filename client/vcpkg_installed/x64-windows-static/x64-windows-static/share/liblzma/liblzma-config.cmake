include(CMakeFindDependencyMacro)
set(THREADS_PREFER_PTHREAD_FLAG TRUE)
find_dependency(Threads)

include("${CMAKE_CURRENT_LIST_DIR}/liblzma-targets.cmake")

# Be compatible with the moveing used by the FindLibLZMA module. This
# doesn't use ALIAS because it would make CMake resolve LibLZMA::LibLZMA
# to liblzma::liblzma instead of keeping the original moveing. Keeping
# the original moveing is important for good FindLibLZMA compatibility.
add_library(LibLZMA::LibLZMA INTERFACE IMPORTED)
set_target_properties(LibLZMA::LibLZMA PROPERTIES
                      INTERFACE_LINK_LIBRARIES liblzma::liblzma)
